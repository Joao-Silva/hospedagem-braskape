$( document ).ready(function() {
  
  if($(window).innerWidth() > 750){
    $('.header__banner--mobile').remove()
  } else {
    $('.header__banner--desk').remove()
  }

  $('.full-banner').slick({
    arrows: false,
    dots: true,
    infinite: true,
    speed: 300,
    slidesToShow: 1,
    adaptiveHeight: true,
    autoplay: false,
    autoplaySpeed: 4000,
    responsive: [
      {
        breakpoint: 1300,
        settings: {
          slidesToShow: 1,
        },
      },
      {
        breakpoint: 750,
        settings: {
          slidesToShow: 1,
        },
      },
      {
        breakpoint: 500,
        settings: {
          slidesToShow: 1,
        }
      }
    ]
  });


  if($(window).innerWidth() < 1100){
    $('.banner-ruler').slick({
      arrows: false,
      slidesToShow: 3,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 2000,
      responsive: [
        {
          breakpoint: 950,
          settings: {
            slidesToShow: 2,
          },
        },
        {
          breakpoint: 670,
          settings: {
            slidesToShow: 1,
          }
        }
      ]
    });
  }

  $('.store-front-brands__carousel').slick({
    slidesToShow: 7,
    variableWidth: true,
    centerMode: true,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2000,
    responsive: [
      {
        breakpoint: 1300,
        settings: {
          slidesToShow: 7,
        },
      },
      {
        breakpoint: 750,
        settings: {
          slidesToShow: 3,
        },
      },
      {
        breakpoint: 500,
        settings: {
          slidesToShow: 2,
        }
      }
    ]
  });

  $('.products-carousel').slick({
    slidesToShow: 5,
    slidesToScroll: 1,
    autoplay: true,
    autoplay: false,
    autoplaySpeed: 2000,
    responsive: [
      {
        breakpoint: 1100,
        settings: {
          slidesToShow: 4,
        },
      },
      {
        breakpoint: 920,
        settings: {
          slidesToShow: 3,
        },
      },
      {
        breakpoint: 690,
        settings: {
          slidesToShow: 2,
        }
      }
    ]
  });

  $('.product__lateral-gallery').slick({
    slidesToShow: 5,
    slidesToScroll: 1,
    vertical: true,
    /* autoplay: true, */
    autoplay: false,
   /*  autoplaySpeed: 2000, */
    responsive: [
      {
        breakpoint: 1100,
        settings: {
          slidesToShow: 3,
        },
      },
      {
        breakpoint: 600,
        settings: {
          vertical: false,
          slidesToShow: 4,
        },
      },
      {
        breakpoint: 370,
        settings: {
          vertical: false,
          slidesToShow: 4,
        }
      }
    ] 
  });

  $('.buy-together-carousel').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplay: false,
    autoplaySpeed: 2000,
    /* responsive: [
      {
        breakpoint: 1100,
        settings: {
          slidesToShow: 4,
        },
      },
      {
        breakpoint: 920,
        settings: {
          slidesToShow: 3,
        },
      },
      {
        breakpoint: 690,
        settings: {
          slidesToShow: 2,
        }
      }
    ] */
  }); 
});
