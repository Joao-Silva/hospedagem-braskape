$( document ).ready(function() {
  $( '.qtd-item__minus' ).click(function(){
    const input_value = Number($(this).siblings('input').val())
    input_value > 1 ? $(this).siblings('input').val(`${input_value - 1}`) : null
  })

  $( '.qtd-item__plus' ).click(function(){
    const input_value = Number($(this).siblings('input').val())
    $(this).siblings('input').val(`${input_value + 1}`)
  })
});
