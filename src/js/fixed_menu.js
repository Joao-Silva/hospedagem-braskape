$(document).ready(function () {
    OrganizarMenu()
    TornarHeaderFixo()
});

$(window).resize(function () {
  OrganizarMenu()
});

function TornarHeaderFixo() {
  //Caso seja menor que 1000px, não faça nada
  if(window.innerWidth <= 1000){
    return
  }

  $(window).scroll(function () { 
      if($(window).scrollTop() === 0)
      {
        $(".header__topbar--fixed").addClass("d-none")
        $(".header__menu").removeClass("fixed")
      }else{
        $(".header__menu").addClass("fixed")
        $(".header__topbar--fixed").removeClass("d-none")
      }
  });
}

function OrganizarMenu() {

    if($(window).innerWidth < 992){
      return
    }

    const largura_container  = $('.categorias_container--sd').width()
    const categorias         = $('.categorias_container--sd > li')
    const margem = 20;

    let largura_categorias = 0
    
    categorias.each(function() {
      if($(this).width() > 0 && $(this).is(':visible')){
        largura_categorias += ($(this).innerWidth() + margem)
      }
    })

    VerificarSeEMaior()
    function VerificarSeEMaior(){
        if (largura_container <= largura_categorias){
            AddAoVerTudo()
        }
    }

    function AddAoVerTudo() {
        largura_categorias = largura_categorias - $(categorias).last().width()

        $('.categorias_container--sd .see-all__submenus')
            .append($('.categorias_container--sd > li').last())

        VerificarSeEMaior()
    }

}